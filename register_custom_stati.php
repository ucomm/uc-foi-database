<?php

// based on http://jamescollings.co.uk/blog/wordpress-create-custom-post-status/

/*
Statuses to conider...

In Progress
Make Public
Publicize


*/
// get all the possible status types.
$post_stati = get_post_stati();

// Register Custom Stati
function custom_post_status_make_public() {

	$args = array(
		'label'                     => _x( 'Make Public', 'Status General Name', 'text_domain' ),
		'label_count'               => _n_noop( 'Make Public (%s)',  'Make Public (%s)', 'text_domain' ),
		'public'                    => true,
		'show_in_admin_all_list'    => true,
		'show_in_admin_status_list' => true,
		'exclude_from_search'       => false,
	);
	register_post_status( 'make_public', $args );

}
add_action( 'init', 'custom_post_status_make_public', 0 );

function custom_post_status_in_progress() {

	$args = array(
		'label'                     => _x( 'In Progress', 'Status General Name', 'text_domain' ),
		'label_count'               => _n_noop( 'In Progress (%s)',  'In Progress (%s)', 'text_domain' ),
		'public'                    => false,
		'show_in_admin_all_list'    => true,
		'show_in_admin_status_list' => true,
		'exclude_from_search'       => true,
	);
	register_post_status( 'in_progress', $args );

}
add_action( 'init', 'custom_post_status_in_progress', 0 );

function filter_post_stati($status) {
	if ($status == 'make_public' || $status == 'in_progress') {
		return $status;
	} else {
		return;
	}
}





function append_post_status_list() {
  global $post;
	$post_type = $post->post_type;
	$post_status = $post->post_status;
	$post_stati = get_post_stati();

  $complete = '';

	$label_array = array();

	$custom_status_array = array_filter($post_stati, 'filter_post_stati');
	$custom_status_values = array_values($custom_status_array);




  if ($post_type == 'request_form') {
		$complete = " selected='selected'";

		foreach ($custom_status_array as $key => $status) {
			$label = "<span id='post-status-display'>".$status."</span>";
			array_push($label_array, $label);

		}
    ?>
    <script>
			jQuery(document).ready(function($) {

				var postStati = <?php echo json_encode($post_stati); ?>;



				var customStatusArray = <?php echo json_encode($custom_status_values);  ?>;
				var labelArray = <?php echo json_encode($label_array); ?>;

				var postStatusElement = $("select#post_status");
				postStatusElement.append("<option value=<?php echo $post_status . $complete. ">".$post_status. '</option>'?>");

				var label = $('.misc-pub-post-status label');

			});
    </script>
    <?php
  }
}
add_action('admin_footer-post.php', 'append_post_status_list');