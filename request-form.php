<?php
/*
Plugin Name: UC FOI Database
Description: Creates a custom post type, integrates with Advanced Custom Fields v5, and exposes two shortcodes (acf_form_generator and results_table).
Author:  UComm Web Team/Adam Berkowitz
Version: 3.2.1
Liscence: MIT
*/

include_once('register_custom_fields.php');
include_once('inc/log_in_check.php');
include_once('inc/register_results_table.php');
include_once('inc/request_form_generator.php');
include_once('inc/admin_view_handler.php');
include_once('inc/file_uploads.php');
include_once('inc/rest_controller.php');

// create a custom post type - request_form
function request_form() {

	$labels = array(
		'name'                  => _x( 'Request Forms', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Request Form', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Request Form', 'text_domain' ),
		'name_admin_bar'        => __( 'Request Form', 'text_domain' ),
		'archives'              => __( 'Request Archive', 'text_domain' ),
		'attributes'            => __( 'Request Attributes', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
		'all_items'             => __( 'All Requests', 'text_domain' ),
		'add_new_item'          => __( 'Add New Request', 'text_domain' ),
		'add_new'               => __( 'New Request', 'text_domain' ),
		'new_item'              => __( 'New Item', 'text_domain' ),
		'edit_item'             => __( 'Edit Request', 'text_domain' ),
		'update_item'           => __( 'Update Request', 'text_domain' ),
		'view_item'             => __( 'View Request', 'text_domain' ),
		'view_items'            => __( 'View Items', 'text_domain' ),
		'search_items'          => __( 'Search Requests', 'text_domain' ),
		'not_found'             => __( 'Request Not Found', 'text_domain' ),
		'not_found_in_trash'    => __( 'Request Not Found in Trash', 'text_domain' ),
		'featured_image'        => __( 'Featured Image', 'text_domain' ),
		'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
		'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
		'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
		'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
		'items_list'            => __( 'Items list', 'text_domain' ),
		'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
	);
	$rewrite = array(
		'slug'                  => 'requests',
		'with_front'            => true,
		'pages'                 => true,
		'feeds'                 => true,
	);
	$args = array(
		'label'                 => __( 'Request Form', 'text_domain' ),
		'description'           => __( 'Request Form', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'trackbacks', 'custom-fields', 'page-attributes', 'post-formats', ),
		'taxonomies'            => array( 'post_tag' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'rewrite'               => $rewrite,
		'capability_type'       => 'page',
		'show_in_rest'          => true,
		'rest_base' 						=> 'requests',
		'rest_controller_class'	=> 'WP_REST_Posts_Controller'
	);
	register_post_type( 'request_form', $args );

}
add_action( 'init', 'request_form', 0 );

/*
	Create custom taxonomies for the posts.
 */
 // Register Custom Taxonomy
 function custom_taxonomies_init() {

	$labels = array(
 		'name'                       => _x( 'Location', 'Taxonomy General Name', 'text_domain' ),
 		'singular_name'              => _x( 'Location', 'Taxonomy Singular Name', 'text_domain' ),
 		'menu_name'                  => __( 'Locations', 'text_domain' ),
 		'all_items'                  => __( 'All Locations', 'text_domain' ),
 		'parent_item'                => __( 'Parent Location', 'text_domain' ),
 		'parent_item_colon'          => __( 'Parent Location:', 'text_domain' ),
 		'new_item_name'              => __( 'New Location Name', 'text_domain' ),
 		'add_new_item'               => __( 'Add New Location', 'text_domain' ),
 		'edit_item'                  => __( 'Edit Location', 'text_domain' ),
 		'update_item'                => __( 'Update Location', 'text_domain' ),
 		'view_item'                  => __( 'View Location', 'text_domain' ),
 		'separate_items_with_commas' => __( 'Separate locations with commas', 'text_domain' ),
 		'add_or_remove_items'        => __( 'Add or remove locations', 'text_domain' ),
 		'choose_from_most_used'      => __( 'Choose from the most used', 'text_domain' ),
 		'popular_items'              => __( 'Popular Locations', 'text_domain' ),
 		'search_items'               => __( 'Search Locations', 'text_domain' ),
 		'not_found'                  => __( 'Not Found', 'text_domain' ),
 		'no_terms'                   => __( 'No locations', 'text_domain' ),
 		'items_list'                 => __( 'Locations list', 'text_domain' ),
 		'items_list_navigation'      => __( 'Locations list navigation', 'text_domain' ),
 	);
 	$args = array(
 		'labels'                     => $labels,
 		'hierarchical'               => true,
 		'public'                     => true,
 		'show_ui'                    => true,
 		'show_admin_column'          => true,
 		'show_in_nav_menus'          => true,
 		'show_tagcloud'              => true,
 	);
 	register_taxonomy( 'location', array( 'request_form' ), $args );
}
add_action( 'init', 'custom_taxonomies_init', 0 );

add_filter('quick_edit_show_taxonomy', function($show, $tax_name, $post_type) {
	if ($post_type !== 'request_form') {
		return;
	}

	if ($tax_name === 'post_tag' || $tax_name === 'location') {
		$show = true;
	}

	return $show;
}, 10, 3);

/*
acf_form_head MUST be called before get_header(). Otherwise the integration plugin doesn't work.
*/

function load_acf_form_head() {
  $template_name = 'acf-integration-template.php';
  $template_folder_path = plugin_dir_path(__FILE__) . 'templates/';
  $template = include $template_folder_path . $template_name;
}

add_filter('wp_enqueue_scripts', 'load_acf_form_head', 0);

function custom_post_order($query) {
	if ($query->get('post_type') === 'request_form') {
		$query->set('orderby', 'ID');
		$query->set('order', 'DESC');
	} 
	return $query;
}
add_action('pre_get_posts', 'custom_post_order');

/**
 * Filter the content of individual requests so that the public info appears.
 *
 * @param string $content
 * @return void
 */
function single_request_content($content) {

	$post_type = get_post_type();

	if ($post_type !== 'request_form') {
		return $content;
	}
	
	include dirname(__FILE__) . '/templates/single-request_form.php';

	return $content;
}

add_filter('the_content', 'single_request_content');

/**
 * 
 * Styles/scripts for the request form etc...
 * 
 */

function foi_enqueue_scripts() {
	global $post;
	$content = $post->post_content;
	if (has_shortcode($content, 'foi_request_log')) {
		wp_enqueue_script('table-script', plugins_url('js/index.js', __FILE__), array('jquery'), null, true);
		wp_enqueue_style('table-style', plugins_url('css/table-styles.css', __FILE__));
	}
	
	if ($post->post_type === 'request_form') {
		wp_enqueue_style('request-post', plugins_url('css/request-post.css', __FILE__));
	}
}
add_action('wp_enqueue_scripts', 'foi_enqueue_scripts', 10, 2);