# UConn FOI Plugin
## 3.2.1
- Improved export functionality to support keyword search and filters
- General updates
## 3.2.0
- Added support for tags in admin area
- Added support for filtering by tag
- Added support for tags and locations in quick edit area
## 3.1.0
- Added affiliation as column in downloadable csv
## 3.0.9
- Fixed which times should be displayed
- Fixed issue with fields not being displayed correctly
## 3.0.8
- Changed start and end times by request
## 3.0.7
- Added location and request start/end dates to request post pages
## 3.0.6
- Updated styles to hide the author by-line for request posts on aurora theme
## 3.0.5
- Added support for displaying request information on individual CPT pages.
## 3.0.4
- Fixed status filter issue
- Updated printed styles
- Added context to printed requests
- Removed emails from logged in user request log
## 3.0.3
- Fixed issue where searches performed on the CPT request_form returned 0 results.
## 3.0.2
- Changed text of one of the "denied" admin reasons.
- Removed one of the "denied" reasons
- Added an "admin notes" area to the exported csv
## 3.0.1
- Fixed style of 
  - table headers
  - pagination numbers
  - link hover states
- added aria-label to email link for logged in users
- fixed the request status field to display status names and not slugs
## 3.0.0
- Completely resived the request log... again. Mostly because the wordpress api was breaking the entire app. Either that or because I'm not good at web development. One or the other ;-) The app is mostly php now with more WP functions and a little JS for interaction. Maybe it'll keep working this time. For a little while. Perhaps...
## 2.0.0
- Completely revised the request log this is a breaking change (hence the 2.0.0)
  - removed react table library in favor of plain jquery
  - implemented REST API controller/endpoint for table
  - users can print individual request details from the request log
- All posts can be exported from the admin panel and downloaded to csv
## 1.0.11
- Fixed a bug where requests were no longer being filtered by status correctly.
## 1.0.10
- Fixed bug where newly submitted requests would be added to the database but not appear in the admin panel
- Fixed some notices due to improperly setting variables
## 1.0.9
- Made first and last names optional
- Admins can filter by request status
- Increased height of the request log to handle longer requests