<?php 

  if (get_field('first_name') !== null) {
    $first_name = get_field('first_name');
  }

  if (get_field('last_name') !== null) {
    $last_name = get_field('last_name');
  }

  $full_name = $first_name . " " . $last_name;

  if (get_field('affiliation') !== null && get_field('affiliation') !== '') {
    $affiliation = get_field('affiliation');
  } else {
    $affiliation = 'Withheld.';
  }

  if (get_field('request_subject') !== null && get_field('request_subject') !== '') {
    $subject = get_field('request_subject');
  } else {
    $subject = 'Withheld.';
  }

  if (get_field('location') !== null && get_field('location') !== '') {
    $location = get_field('location');
    $location_name = $location->name;
  } else {
    $location_name = 'Witheld.';
  }

  if (get_field('request_time_range_start') !== null && get_field('request_time_range_start') !== '') {
    $time_start = get_field('request_time_range_start');
  } else {
    $time_start = 'Withheld.';
  }

  if (get_field('request_time_range_end') !== null && get_field('request_time_range_end') !== '') {
    $time_end = get_field('request_time_range_end');
  } else {
    $time_end = 'Withheld.';
  }

  // the choice is a key in an array of choices.
  // set the status to the value retrieved by the key.
  if (get_field_object('request_status', $id) && get_field('request_status') !== null) {
    $field_object = get_field_object('request_status', $id);
    $choices_array = $field_object['choices'];
    $choice = get_field('request_status');
    $status = $choices_array[$choice];
  }

  if (get_field('request_details') !== null) {
    $details = get_field('request_details');
  }

  if (get_field('public_notes') !== null) {
    $notes = get_field('public_notes');
  }
?>

<h2><?php echo $subject; ?></h2>
<div class="details-container">
  <p><strong>Name:</strong> <?php echo $full_name; ?></p>
  <p><strong>Affiliation:</strong> <?php echo $affiliation; ?></p>
  <p><strong>Location:</strong> <?php echo $location_name; ?></p>
  <p><strong>Request Date Start:</strong> <?php echo $time_start; ?></p>
  <p><strong>Request Date End:</strong> <?php echo $time_end; ?></p>
  <p><strong>Details:</strong> <?php echo $details; ?></p>
  <p><strong>Notes:</strong> <?php echo $notes; ?></p>
</div>
