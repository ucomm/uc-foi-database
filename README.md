UConn Freedom of Information (after here FOI) Plugin
====================================================

# Environment set up

1. Fork or clone the transparency-aurora-localdev directory to begin development
1. Use yarn.js to install javascript dependencies. If you don't have it, `npm install -g yarn`.
1. Install JS dependencies -> `yarn install`. The `yarn.lock` file will make sure you have the same versions as were used to develop this.

**The FOI plugin requires two directories and several files for development.**

- `[root-directory-name]` is the WP plugin and _compiled_ react application (in `[root-directory-name]/js`)
- `[root-directory-name]/.results-table` This is the react application that renders the results table.
- `[root-directory-name]/webpack.config.js` This will compile the react app and place the results in `[root-directory-name]/js` as `app.js`.

### Directory Structure
```
root
  |__ .results-table // react app files.
  |__ docker-compose.yml
  |__ webpack.config.js
  |__ yarn.lock (don't change)
  |__ package.json
  |__ www
      |__ wordpress/wp-content/plugins/uc-foi-database
```
# Local Development
Work on separate branches using git flow. If you don't have it, install with homebrew.

For work on the request form, use `docker-compose up`. docker-compose can be installed with homebrew.

For work on the results table use `npm run webpack-watch`. This will run webpack continuously and watch for file changes. Don't use this for production. Every time you save, it will re-compile the application and store a new version in `uc-foi-database/js`.

***Important!***
Before committing react code, kill the webpack process in the terminal. Failure to do so will almost certainly cause problems with merge conflicts.

If npm throws an error, check that the following scripts are in the `package.json` file. Also, check that you're in the root directory in the terminal and haven't navigated out of it.

```
"scripts": {
  "dev-server": "./node_modules/.bin/webpack-dev-server --content-base build --inline --hot --port 3000",
  "webpack-watch": "webpack --watch",
  "build": "webpack -p"
}
```
I'm not going to get into a whole thing about webpack here, but if you _really_ want to move the table application folder, make sure you change the `webpack.config.js` file accordingly.

# Dev Server

[transparency dev site](http://communicationswp.dev.uconn.edu/transparency/)
The process on the Jenkins server is called `transparency plugin - Push to Sandbox`.

Pushing code to the develop branch will start a Jenkins build. It has yarn and all the dependencies already. The versions should all be the same (gotta love lock files).

## Usage
### Getting started
```bash
$ composer install # only the first time
$ docker-compose up
```
### Accessing containers
To access a particular docker container, find the container name and then enter an interactive terminal.
```bash
$ docker ps # to get the container name
$ docker exec -it container_name /bin/bash
```
### Debugging Wordpress
Wordpress debug logs can be found inside the web container at `/etc/httpd/logs/error_log`

## Testing against Castor and Lobo
This repo purposely doesn't require castor or lobo as dev dependencies. To check compatibility, take the following steps
- Update this repo with the latest version of bb-plugin
- Run the pipeline process for the [composer repository](https://bitbucket.org/ucomm/composer-repository/addon/pipelines/home#!/)
- In castor or lobo, run `composer update` to pull in the new version. 

## Bitbucket
### Creating releases
Assuming you're using git flow, tag the release with the command `git flow release start {version_number}`. Tags must follow the [semver system](http://semver.org/). Follow these steps to complete a release
**NB - Beaver Builder doesn't follow semver and frequently uses 4 numbers for versions. This scheme won't work with our satis repo. Make sure to check our tags and don't follow theirs.**
```bash
git tag # check the current tags/versions on the project
git flow release start {new_tag}
git flow release finish {new_tag}
# in the first screen add new info or just save the commit message as is
# in the second screen type in the same tag you just used and save.
git push --tags && git push origin master
git checkout develop
``` 
Finally re-run the pipeline build on the [satis repo](https://bitbucket.org/ucomm/composer-repository).
### Pipelines
This repo has a bitbucket pipelines (written by Adam Berkowitz) attached in case you wish to create zip file downloads for tags/branches. **You may exclude files/folders from the zip archive by adding them to composer.json. The syntax is the same as gitignore.** To enable pipelines on bitbucket, simply visit the project repo and enable pipelines at `repo -> settings -> Pipelines/settings`.
## Dependency management

It would be a good idea to keep UComm/WordPress dependencies in require-dev, and only keep functional dependencies in "require".  That way, your package will export with only the required files for your plugin/theme to function, and not include a full WP install. The only current non-dev requirement is the uconn/banner package.
