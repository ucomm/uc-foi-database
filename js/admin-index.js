// create a button for users to click and add it to the admin panel for requests.

(function($) {
  const postQuerySubmit = $('#post-query-submit');
  const exportButton = $("<input type='submit' name='export_requests_to_csv' id='export-requests' class='button button-primary' value='Export All Requests' />");
  exportButton.insertAfter(postQuerySubmit);

  // recreate the search results text after searches.
  const { location: { search } } = window;
  const s = search.match(/^\?s=([a-zA-Z1-9%+]*)/);

  if (s) {
    const term = s[1].replace('+', ' ');
    const text = 'Search results for "' + term + '"'
  
    const subtitle = $('.subtitle');
    subtitle.text(text);
  }
  
})(jQuery);