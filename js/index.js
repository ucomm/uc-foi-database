(function($) {
  var table = $('#uc-foi-body-content');
  var detailsContainer = $('.uc-foi-details-container');
  var clearSearch = $('#uc-foi-clear-search');
  var selector;
  var overlay;
  $(table).on('click', '.uc-foi-modal-open', function(evt) {
    var target = evt.target;
    var id = target.id;
    selector = '[data-detail="' + id + '"]';    
    overlay = $(selector);

    overlay.toggleClass('uc-foi-hidden');
    evt.preventDefault();
  });

  $(detailsContainer).on('click', function(evt) {
    var target = evt.target;

    if (target.localName !== 'button') return;

    if (target.className === 'uc-foi-detail-close') {
      overlay.toggleClass('uc-foi-hidden');
    } else if (target.className === 'uc-foi-detail-print'){
      window.print();
    }
    
    evt.preventDefault();
  })

  // redirect back to the initial page when the search is cleared.
  $(clearSearch).on('click', function (evt) {
    window.location.href = '/request-log';
    evt.preventDefault();
  });

})(jQuery)