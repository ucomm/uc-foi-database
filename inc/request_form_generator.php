<?php

/*
=========================================================================
Turn the form into a shortcode that can be added to pages.
1 - gather the options for the form and pass them to acf_form()
2 - create the WP shortcode with defualts for options.
-------
The values filled out in the front-end form will be saved as new draft posts in the database. Can be published at the discretion of the admins.
=========================================================================
*/

// $category = get_field('location');


/**
 * Include the plugin helpers - No idea why this is needed. Bug in PHP 5.3.3 or latest ACF Pro?
 * @link https://support.advancedcustomfields.com/forums/topic/fatal-error-call-to-undefined-function-is_plugin_active/
 */
if( !function_exists('is_plugin_active') ) {
    include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
}

function output_acf_form($submit, $redirect) {
	if (!is_plugin_active('advanced-custom-fields-pro/acf.php')) {
		echo "<div class='acfError'><p><strong>The request form is currently broken. Please contact the system administrator.</strong></p></div>";
	} else {
			$location = get_field('location');
			date_default_timezone_set('US/Eastern');
		  $date = date("m-j-y, g:i a");

			$options = array(
				'post_id' => 'new_post',
				'field_groups' => array('group_5888d67d08c27'),
				'new_post' => array(
					'post_type' => 'request_form',
					'post_status' => 'draft',
					'post_title' => $date,
					'post_category' => array($location)
				),
				'return' => home_url($redirect),
				'submit_value' => $submit
			);

		  $content = acf_form($options);
		  return $content;
	}
}

function acf_form_shortcode($atts, $content = null) {
	// use output buffering to make the text of the page appear before the form.
	ob_start();

  $opts = shortcode_atts(array(
    'submit' => 'Make a Request',
    'redirect' => ''
  ), $atts);

  $submit = $opts['submit'];
  $redirect = $opts['redirect'];

	output_acf_form($submit, $redirect);

	return ob_get_clean();

}
add_shortcode('foi_request_form', 'acf_form_shortcode');

// function pre_save_request_form($post_id) {
// 	// $category = get_field('organization');
// 	if ($post_id !== 'new_post') {
// 		return $post_id;
// 	}
// 	if (is_admin()) {
// 		return;
// 	}
//
// 	date_default_timezone_set('US/Eastern');
// 	$date = date("j-m-y, g:i a");
//
// 	$post = array(
// 		'post_status' => 'draft',
// 		'post_title' => $date,
// 		'post_type' => 'request_form',
// 		// 'post_category' => array($category)
// 	);
// 	$post_id = wp_insert_post($post);
// 	return $post_id;
// }
// add_filter('acf/pre_save_post', 'pre_save_request_form');



function email_auto_responder($post_id) {
	if (get_post_type($post_id) !== 'request_form') {
		return;
	}
	if (is_admin()) {
		return;
	}

	$post = get_post($post_id);
	$subject = 'Your FOI request';
	$to = get_field('email', $post_id);
	$from = get_option('admin_email');
	$headers = 'Content-type: text/html; charset=UTF-8' . "\r\n";
	$headers .= 'From: UConn FOI <' . $from .'>' . "\r\n";
	$headers .= "BCc: $from" . "\r\n";

	$body = "This is to confirm receipt of your request for records from the University of Connecticut pursuant to the Connecticut Freedom of Information Act (FOIA). Your request will be provided to appropriate University personnel to identify and compile the existing and responsive documents. Once compiled, the records will then be reviewed as to any applicable exemptions under the FOIA that might be cause for non-disclosure or redaction of certain documents as per Connecticut law. Once the records relevant to your request have been compiled and reviewed, we will contact you regarding any possible charges and the most appropriate manner of transferring the documents to you. <br /><br />";
	$body .= 'Please address any questions as to your request or any related matter to <a href=mailto:' . $from . '>UConn FOI</a><br /><br />';
	$body .= 'Thank you for your correspondence.';

	wp_mail($to, $subject, $body, $headers);

}
add_action('acf/save_post', 'email_auto_responder');
