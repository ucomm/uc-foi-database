<?php
// If the post type is 'request_form', these functions move the acf fields above the wysiwyg editor and remove several metaboxes created by plugins.

function the_current_screen() {
	$screen = get_current_screen();
	if ($screen->post_type === 'request_form') {
		function move_wysiwyg_editor() {
			global $post;
			global $wp_meta_boxes;
			do_meta_boxes(get_current_screen(), 'normal', $post);
			unset($wp_meta_boxes[get_post_type($post)]['normal']);
		}
		add_action('edit_form_after_title', 'move_wysiwyg_editor');

		function remove_plugin_metaboxes() {
			remove_meta_box('wds_seomoz_urlmetrics', 'request_form', 'advanced');
			remove_meta_box('mymetabox_revslider_0', 'request_form', 'advanced');
			remove_meta_box('wds-wds-meta-box', 'request_form', 'advanced');
			remove_meta_box('locationdiv', 'request_form', 'side');
		}
		add_action('do_meta_boxes', 'remove_plugin_metaboxes');
	}
}
add_action('current_screen', 'the_current_screen');


function get_post_term_name() {
	global $post;

	if(isset($post)){
			$post_ID = $post->ID;

		$terms_array = wp_get_post_terms($post_ID, 'location');
		/*
		for now, since users will only be able to select one location per request, I'm accessing the array directly.
		If this changes a foreach loop may be needed.
		*/
		$post_term_name = is_object($terms_array) ? $terms_array[0]->name : null;
	}
}
add_action('admin_footer', 'get_post_term_name');

function filter_forms_by_location() {
	global $typenow;
	$args = array(
		'public' => true,
		'_builtin' => false
	);
	$post_types = get_post_types($args);
	if ( in_array($typenow, $post_types) ) {
		$filters = get_object_taxonomies($typenow);
		foreach ($filters as $tax_slug) {
				$tax_obj = get_taxonomy($tax_slug);
				if (isset($_GET[$tax_obj->name])) {
					$selected = $_GET[$tax_obj->name];
				} else {
					$selected = '';
				}
				wp_dropdown_categories(array(
						'show_option_all' => __('All '. $tax_obj->labels->menu_name ),
						'taxonomy' => $tax_slug,
						'name' => $tax_obj->name,
						'orderby' => 'term_order',
						'selected' => $selected,
						'hierarchical' => $tax_obj->hierarchical,
						'show_count' => false,
						'hide_empty' => false
				));
			}
	}
}
add_action('restrict_manage_posts', 'filter_forms_by_location');

function filter_forms_by_status() {
	global $post_type;

	if ($post_type != 'request_form') {
		return;
	} else {

		/*
		*	Because of the way this plugin works, we have to do something a little odd. 
		*	Basically this code will store the status_array in the database because it doesn't exist in the database to begin with.
		* If you try to access it via wpdb query without this, it'll fail. Also if you try to just get it from get_field_object it'll fail.
		*/

		$status_array = [];
		$result = get_field_object('request_status');
		$choices = $result['choices'];

		if (!get_option('status_array') || empty(get_option('status_array'))) {
			update_option('status_array', $choices);
		}

		$status_array = get_option('status_array');

		echo "<select class='postform' id='status-filter' name='status'>";
		foreach ($status_array as $status_key => $status) {
			if (strlen($status) == 0) {
				$status = 'All Request Statuses';
			}
			echo "<option value=$status_key>$status</option>";
		}
		echo "</select>";
	}

	$status_filter = '';
	if (isset($_GET['status'])) {
		$status_filter = $_GET['status'];
	}
	?>
		<script>
			var statusFilter = document.getElementById('status-filter');
			var statusFilterValue = "<?php echo $status_filter ?>";
			statusFilter.value = statusFilterValue;
		</script>
	<?php
}
add_action('restrict_manage_posts', 'filter_forms_by_status');

function query_posts_by_status($query) {
	global $typenow;
	$request_status = '';
	if (is_admin() && $typenow == 'request_form' && (isset($_GET['status']) && $_GET['status'] !== '')) {
		$request_status = $_GET['status'];	
		$query->query_vars['meta_key'] = 'request_status';
		$query->query_vars['meta_value'] = $request_status;
	}
	return $query;
};
add_filter('parse_query', 'query_posts_by_status');

function query_posts_by_tags($query) {
	global $typenow;
	if (is_admin() && $typenow === 'request_form' && (isset($_GET['post_tag']) && $_GET['post_tag'] !== '')) {
		$query->query_vars['tag_id'] = $_GET['post_tag'];
	}
	return $query;
}

add_filter('parse_query', 'query_posts_by_tags');

function query_posts_by_location($query) {
	global $pagenow;
	global $typenow;
	if ($pagenow == 'edit.php') {
			$filters = get_object_taxonomies($typenow);
			foreach ($filters as $tax_slug) {
					$var = & $query->query_vars[$tax_slug];
					if ( isset($var) && $var > 0) {
							$term = get_term_by('id', $var, $tax_slug);
							$var = $term->slug;
					}
			}
	}
	return $query;
}
add_filter('parse_query','query_posts_by_location');

// Set the headers for each column.
function set_column_headers($columns) {
	$columns = array(
		'cb' => '<input type="checkbox" />',
		'title' => __('Title'),
		'name' => __('Name'),
		'subject' => __('Subject'),
		'details' => __('Details'),
		'taxonomy-location' => __('Location'),
		'tags' => __('Tags'),
		'status' => __('Status'),
		'date-begun' => __('Date Begun'),
		'date-complete' => __('Date Completed')
	);
	return $columns;
}
add_action('manage_edit-request_form_columns', 'set_column_headers');

function manage_request_form_columns($column, $post_id) {
	global $post;
	$post_id = $post->ID;
	$fields = get_fields($post_id);

	switch ($column) {
		case 'name' :
			if (isset($fields['first_name'])) {
				$first_name = $fields['first_name'];
			} else {
				$first_name = '';
			}
			if (isset($fields['last_name'])) {
				$last_name = $fields['last_name'];
			} else {
				$last_name = '';
			}
			echo "$first_name $last_name";
			break;
		case 'subject':	
			if (isset($fields['request_subject'])) {
				$subject = $fields['request_subject'];
			} else {
				$subject = '';
			}
			echo $subject;
			break;
		case 'details':

			if (isset($fields['request_details'])) {
				$details = $fields['request_details'];
			} else {
				$details = '';
			}

			if (strlen($details) > 140) {
				$details = substr($details, 0, 140) . '...';
			}
			echo $details;
			break;
		case 'status':
			if (isset($fields['request_status'])) {
				$status_key = $fields['request_status'];
				$status = get_field_object('request_status');
				$status = $status['choices'][$status_key];
			}
			if (empty($status)) {
				echo '';
			} else {
				echo $status;
			}
			break;
		case 'date-begun':
			if (isset($fields['date_begun'])) {
				$begun = $fields['date_begun'];
			} else {
				$begun = '';
			}
			echo $begun;
			break;
		case 'date-completed':
			if (isset($fields['date_completed'])) {
				$completed = $fields['date_completed'];
			} else {
				$completed = '';
			}
			echo $completed;
			break;
		default:
			break;
	}
}
add_action('manage_request_form_posts_custom_column', 'manage_request_form_columns', 10, 2);

/**
 * allow JS to
 * 	- add an export button to the admin dashboard
 * 	- recreate the search results text.
 * @return void
 */
function admin_js() {
	global $post_type;

	if ($post_type !== 'request_form') {
		return;
	} else {
		wp_enqueue_script('admin-index', plugins_url('../js/admin-index.js', __FILE__), array('jquery'), wp_get_theme()->get('Version'), true);
	}
}

add_action('restrict_manage_posts', 'admin_js');

/**
 * This function takes over the search query so it only searches ACF meta fields.
 * Otherwise no results are found because WP searches against 
 * 	- title
 * 	- content
 * 	- excerpt 
 * None of these are present in the CPTs and so without this function searches fail.
 *
 * @param object $query
 * @return object
 */
function meta_field_search($query) {
	if (!is_admin()) {
		return;
	}

	if ($query->query['post_type'] !== 'request_form') {
		return;
	}

	if (!$query->is_search) {
		return $query;
	}

	$search = $query->query_vars['s'];

	$meta_query = array(
		'relation' => 'OR',
		array(
			'key' => 'first_name',
			'value' => sanitize_text_field($search),
			'compare' => 'IN'
		),
		array(
			'key' => 'last_name',
			'value' => sanitize_text_field($search),
			'compare' => 'IN'
		),
		array(
			'key' => 'affiliation',
			'value' => sanitize_text_field($search),
			'compare' => 'LIKE'
		),
		array(
			'key' => 'request_subject',
			'value' => sanitize_text_field($search),
			'compare' => 'LIKE'
		),
		array(
			'key' => 'request_details',
			'value' => sanitize_text_field($search),
			'compare' => 'LIKE'
		),
		array(
			'key' => 'request_status',
			'value' => sanitize_text_field($search),
			'compare' => 'LIKE'
		)
	);

	$query->set('meta_query', $meta_query);
	$query->set('s', '');

	return $query;
}

add_action('pre_get_posts', 'meta_field_search');

function export_all_requests() {
	if (isset($_GET['export_requests_to_csv'])) {
		global $post;
		$args = array(
			'post_type' => 'request_form',
			'post_status' => array(
				'publish',
				'pending',
				'draft',
				'private'
			),
			'numberposts' => -1
		);

		// filter by keyword search
		if (isset($_GET['s']) && '' !== $_GET['s']) {
			$args['s'] = $_GET['s'];
		}

		// filter by month/year
		if (isset($_GET['m']) && '0' !== $_GET['m']) {
			$args['m'] = intval($_GET['m']);
		}

		// prepare to filter by tax query
		if ($_GET['post_tag'] !== '0' || $_GET['location'] !== '0') {
			$args['tax_query'] = [];

			if ($_GET['post_tag'] !== '0') {
				$tag_query = [
					'taxonomy' => 'post_tag',
					'field' => 'term_id',
					'terms' => intval($_GET['post_tag'])
				];
				array_push($args['tax_query'], $tag_query);
			}

			if ($_GET['location'] !== '0') {
				$loc_query = [
					'taxonomy' => 'location',
					'field' => 'term_id',
					'terms' => intval($_GET['location'])
				];
				array_push($args['tax_query'], $loc_query);
			}

			if (count($args['tax_query']) > 1) {
				$args['tax_query']['relation'] = 'AND';
			}
		}

		// prepare status meta query
		if (isset($_GET['status']) && '' !== $_GET['status']) {
			$args['meta_query'] = [
				[
					'key' => 'request_status',
					'value' => $_GET['status']
				]
			];
		}

		$posts = get_posts($args);
		if (!$posts) {
			return;
		} else {
			$date = date('Y-m-d');
			$filename = "foi-requests-$date.csv";
			header('Content-type: text/csv');
			header("Content-Disposition: attachment; filename=$filename");
			header('Pragma: no-cache');
			header('Expires: 0');

			$file = fopen('php://output', 'w');
			$file_headings = array(
				'Title',
				'Name',
				'Affiliation',
				'Subject',
				'Details',
				'Location',
				'Status',
				'Date Begun',
				'Date Completed',
				'Admin Notes'
			);
			fputcsv($file, $file_headings);
			foreach ($posts as $index => $post) {
				$post_id = $post->ID;
				$fields = get_fields($post_id);
				$title = get_the_title($post_id);
				
				if (isset($fields['first_name'])) {
					$first_name = $fields['first_name'];
				} else {
					$first_name = '';
				}
				
				if (isset($fields['last_name'])) {
					$last_name = $fields['last_name'];
				} else {
					$last_name = '';
				}
				$full_name = "$first_name $last_name";
				
				if (isset($fields['affiliation'])) {
					$affiliation = $fields['affiliation'];
				} else {
					$affiliation = '';
				}

				if (isset($fields['request_subject'])) {
					$subject = $fields['request_subject'];
				} else {
					$subject = '';
				}

				if (isset($fields['request_details'])) {
					$details = wp_strip_all_tags($fields['request_details']);
				} else {
					$details = '';
				}

				$terms = get_the_terms($post_id, 'location');
				$location = $terms[0]->name;

				if (isset($fields['request_status'])) {
					$status_key = $fields['request_status'];
					$status = get_field_object('request_status');
					$status = $status['choices'][$status_key];
				}
				if (empty($status)) {
					$status = '';
				}

				if (isset($fields['date_begun'])) {
					$begun = $fields['date_begun'];
				} else {
					$begun = '';
				}

				if (isset($fields['date_completed'])) {
					$completed = $fields['date_completed'];
				} else {
					$completed = '';
				}

				if (isset($fields['administrative_notes'])) {
					$admin_notes = $fields['administrative_notes'];
				} else {
					$admin_notes = '';
				}

				fputcsv($file, array(
					$title,
					$full_name,
					$affiliation,
					$subject,
					$details,
					$location,
					$status,
					$begun,
					$completed,
					$admin_notes
				));
			}
			fclose($file);
			exit();
		}
	}
}

add_action('admin_init', 'export_all_requests');