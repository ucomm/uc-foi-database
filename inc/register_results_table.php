<?php

function results_table_shortcode($atts, $content = null) {
	global $wp_query;
	$is_logged_in = is_user_logged_in();
	$original_query = $wp_query;

	$html = '';
	include(plugin_dir_path(__DIR__) . 'templates/search-form.php');
									
	$table = "<table id='uc-foi-table'>
							<thead id='uc-foi-table-headings'>
								<tr id='uc-foi-headings'>
									<td id='uc-foi-name-header' class='uc-foi-header-item'>Name</td>
									<td id='uc-foi-affiliation-header' class='uc-foi-header-item'>Affiliation</td>
									<td id='uc-foi-subject' class='uc-foi-header-item'>Subject</td>
									<td id='uc-foi-location' class='uc-foi-header-item'>Location</td>
									<td id='uc-foi-status' class='uc-foi-header-item'>Request Status</td>
								</tr>
							</thead>
							<tbody id='uc-foi-body-content'>";
							
	// create a query for the request_form posts and loop through it.
	$args = array(
		'paged' => get_query_var('paged'),
		'post_type' => 'request_form',
		'posts_per_page' => 10
	);

	// if the user searches, get the search term and display all results on one page.
	if (isset($_GET['search'])) {
		$request = sanitize_text_field($_GET['search']);
		$search = urldecode($request);
		$args['paged'] = 1;
		$args['posts_per_page'] = -1;
		$args['meta_query'] = construct_meta_query_array($search);
	}

	$query = new WP_QUERY($args);

	$wp_query = $query;

	while ($query->have_posts()) {
		$query->the_post();
		$id = get_the_ID();
		$title = get_the_title();
		$location_field = get_field('location');
		$first_name = '';
		$last_name = '';
		$full_name = '';
		// $email = '';
		$affiliation = '';
		$subject = '';
		$location = '';
		$status = '';
		$details = '';
		$notes = '';

		if (get_field('first_name') !== null) {
			$first_name = get_field('first_name');
		}

		if (get_field('last_name') !== null) {
			$last_name = get_field('last_name');
		}

		$full_name = $first_name . " " . $last_name;

		if (get_field('affiliation') !== null) {
			$affiliation = get_field('affiliation');
		}

		if (get_field('request_subject') !== null) {
			$subject = get_field('request_subject');
		}

		if ($location_field) {
			// if there's no location get_field('location') just returns false.
			$location = $location_field->name;
		}

		// the choice is a key in an array of choices.
		// set the status to the value retrieved by the key.
		if (get_field_object('request_status', $id) && get_field('request_status') !== null) {
			$field_object = get_field_object('request_status', $id);
			$choices_array = $field_object['choices'];
			$choice = get_field('request_status');
			$status = $choices_array[$choice];
		}

		if (get_field('request_details') !== null) {
			$details = get_field('request_details');
		}

		if (get_field('public_notes') !== null) {
			$notes = get_field('public_notes');
		}
		// create a row for each request
		$table .= "<tr>
								<td>$full_name</td>
								<td>$affiliation</td>
								<td><a id='request-$id' href='#' class='uc-foi-modal-open' aria-label='Read more request details about $subject'>$subject</a></td>
								<td>$location</td>
								<td>$status</td>
							</tr>";
		// create a modal for each request
		$table .= "<div data-detail='request-$id' class='uc-foi-overlay uc-foi-hidden'>
								<div class='uc-foi-details-container uc-foi-modal-window'>
									<div class='uc-foi-print-container'>
										<strong class='uc-foi-detail-title'>$title</strong><span> - </span><span class='uc-foi-detail-subject'>Subject - $subject</span>
										<p class='uc-foi-detail-name'>Name - $full_name</p>
										<p class='uc-foi-detail-affiliation'>Affiliation - $affiliation</p>
										<p class='uc-foi-detail-content'>$details</p>
										<hr />
										<p class='uc-foi-detail-notes'>$notes</p>
									</div>
									<button class='uc-foi-detail-close'>close</button>
									<button class='uc-foi-detail-print'>print</button>
								</div>
							</div>
							";
	}
	// exit the loop and close the table
	$table .= "</tbody>
	</table>";

	$pagination_args = array(
		'prev_text' => 'Previous Requests',
		'next_text' => 'Next Requests'
	);
	the_posts_pagination($pagination_args);
	wp_reset_postdata();
	$wp_query = $original_query;

	$html .= $table;
	echo $html;
}

add_shortcode('foi_request_log', 'results_table_shortcode');

function construct_meta_query_array($search) {
    $meta_query_array = array(
      'relation' => 'OR',
        array(
          'key' => 'first_name',
          'value' => sanitize_text_field($search),
          'compare' => 'IN'
        ),
        array(
          'key' => 'last_name',
          'value' => sanitize_text_field($search),
          'compare' => 'IN'
        ),
        array(
          'key' => 'affiliation',
          'value' => sanitize_text_field($search),
          'compare' => 'LIKE'
        ),
        // array(
        //   'key' => 'location',
        //   'value' => sanitize_text_field($search),
        //   'compare' => 'LIKE'
        // ),
        array(
          'key' => 'request_subject',
          'value' => sanitize_text_field($search),
          'compare' => 'LIKE'
        ),
        array(
          'key' => 'request_details',
          'value' => sanitize_text_field($search),
          'compare' => 'LIKE'
        ),
        array(
          'key' => 'request_status',
          'value' => sanitize_text_field($search),
          'compare' => 'LIKE'
        )
      );
    return $meta_query_array;
  }