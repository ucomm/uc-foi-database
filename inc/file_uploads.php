<?php
// allow additional file types to be uploaded via ACF file upload field.

function custom_upload_mimes($existing_mimes=array()) {
  $existing_mimes['zip'] = 'application/zip';
  return $existing_mimes;
}

add_filter('upload_mimes', 'custom_upload_mimes');
