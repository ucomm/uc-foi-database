<?php

function log_in_check() {
	return is_user_logged_in();
}
add_action('init', 'log_in_check');
