<?php

// http://localhost/wp-json/wp/v2/requests
// new
// http://localhost/wp-json/uconn/v1/requests
// keeping this for reference
/*
include_once('log_in_check.php');

class UC_FOI_Controller extends WP_REST_Posts_Controller {
  public function __construct() {
    $this->namespace = '/uconn/v1';
    $this->resource_name = 'requests';
    $this->post_type = 'request_form';
    $this->meta = new WP_REST_Post_Meta_Fields($this->post_type);
    $this->is_logged_in = log_in_check();
  }

  public function register_routes() {
    // GET 10 requests at a time.
    register_rest_route($this->namespace, '/' . $this->resource_name, array(
      array(
        'methods' => 'GET',
        'callback' => array($this, 'get_items')
      )
    ));
    // GET a single request by post ID
    register_rest_route($this->namespace, '/' . $this->resource_name . '/(?P<id>[\d]+)', array(
      array(
        'methods' => 'GET',
        'callback' => array($this, 'get_single_item')
      )
    ));
  }

  public function get_items($request) {
    $page = 1;
    $post_status = array();

    if (!$this->is_logged_in) {
      $post_status = array('publish');
    } else {
      $post_status = array('publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit', 'trash');
    }
    if (isset($_GET['page'])) {
      $page = $_GET['page'];
    }

    $query_args = array(
      'post_type' => 'request_form',
      'numberposts' => 10,
      'paged' => $page,
      'post_status' => $post_status
    );
    
    if (isset($_GET['s'])) {
      $request = sanitize_text_field($_GET['s']);
      $search = urldecode($request);
      $query_args['meta_query'] = $this->construct_meta_query_array($search);
    }

    $query = new WP_Query($query_args);
    $posts = $query->posts;

    $posts_array = array();
    wp_reset_postdata();
    foreach ($posts as $index => $post) {
      // combine the post data and acf fields into one array.
      $fields = get_fields($post->ID);
      $fields['ID'] = $post->ID;
      $request_status_object = get_field_object('request_status', $post->ID);
      $fields['request_status_object'] = $request_status_object['choices'];
      array_push($posts_array, $fields);
    }
    return $posts_array;
  }

  public function get_single_item($request) {
    $post_id = $request->get_param('id');
    $request_array = array(
      'request_title' => get_the_title($post_id),
      'first_name' => get_field('first_name', $post_id),
      'last_name' => get_field('last_name', $post_id),
      'request_details' => strip_tags(get_field('request_details', $post_id)),
      'request_subject' => get_field('request_subject', $post_id),
      'public_notes' => get_field('public_notes', $post_id)
    );
    return $request_array;
  }

  private function construct_meta_query_array($search) {
    $meta_query_array = array(
      'relation' => 'OR',
        array(
          'key' => 'first_name',
          'value' => sanitize_text_field($search),
          'compare' => 'IN'
        ),
        array(
          'key' => 'last_name',
          'value' => sanitize_text_field($search),
          'compare' => 'IN'
        ),
        array(
          'key' => 'affiliation',
          'value' => sanitize_text_field($search),
          'compare' => 'LIKE'
        ),
        array(
          'key' => 'location',
          'value' => sanitize_text_field($search),
          'compare' => 'LIKE'
        ),
        array(
          'key' => 'request_subject',
          'value' => sanitize_text_field($search),
          'compare' => 'LIKE'
        ),
        array(
          'key' => 'request_details',
          'value' => sanitize_text_field($search),
          'compare' => 'LIKE'
        ),
        array(
          'key' => 'request_status',
          'value' => sanitize_text_field($search),
          'compare' => 'LIKE'
        )
      );
    return $meta_query_array;
  }
}

function foi_register_rest_routes() {
  $controller = new UC_FOI_Controller();
  $controller->register_routes();
}
add_action('rest_api_init', 'foi_register_rest_routes');
*/